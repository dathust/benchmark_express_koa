FROM node:12.18-alpine

WORKDIR /app

RUN npm cache clean --force
RUN rm -rf node_modules

# instal npm
COPY package.json package-lock.json ./
RUN npm ci --production=true

COPY . .

CMD ["npm", "run", "express"]
